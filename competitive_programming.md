# Competitive Programming

## Definition

A mind sport with participants trying to program according to provided specification.

### Puzzle

Logical or mathematical problems

### Judging

[How to judge a solution?]

#### Problems Solved

#### Output Quality

#### Execution Time

#### Program Size

## Competitions

### Kaggle

### Google Code Jam

### Topcoder

> https://en.wikipedia.org/wiki/Competitive_programming#:~:text=Competitive%20programming%20is%20a%20mind,referred%20to%20as%20sport%20programmers.
